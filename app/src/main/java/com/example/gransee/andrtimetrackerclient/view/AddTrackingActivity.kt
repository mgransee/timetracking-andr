package com.example.gransee.andrtimetrackerclient.view


import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import com.example.gransee.andrtimetrackerclient.R
import com.example.gransee.andrtimetrackerclient.model.Customer
import com.example.gransee.andrtimetrackerclient.model.Job
import com.example.gransee.andrtimetrackerclient.model.Project
import com.example.gransee.andrtimetrackerclient.model.Role
import com.example.gransee.andrtimetrackerclient.repositories.*
import kotlinx.android.synthetic.main.activity_add_tracking.*
import okhttp3.RequestBody
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.time.Year
import java.util.*


/**
 * Created by gransee on 24.09.2018.
 */
class AddTrackingActivity : AppCompatActivity() {

    var customers: List<Customer> = emptyList()
    var projects: List<Project> = emptyList()
    var roles: List<Role> = emptyList()
    var tasks: List<Job> = emptyList()
    var selectedCustomerId: String = ""
    var selectedProjectId: String = ""
    var selectedRoleId: String = ""
    var selectedJobId: String = ""
    var selectedStartTime: Long = 0
    var selectedEndTime: Long = 0

    val calendarStart = Calendar.getInstance(TimeZone.getTimeZone("GMT+2:00"), Locale.GERMANY)
    val calendarEnd = Calendar.getInstance(TimeZone.getTimeZone("GMT+2:00"), Locale.GERMANY)

    fun showToast(context: Context = applicationContext, message: String, duration: Int = Toast.LENGTH_SHORT) {
        Toast.makeText(context, message, duration).show()
    }

    fun createJsonRequestBody(vararg params: Pair<String, Any>) =
            RequestBody.create(
                    okhttp3.MediaType.parse("application/json; charset=utf-8"),
                    JSONObject(mapOf(*params)).toString())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_tracking)

        selectedCustomerId = ""
        selectedProjectId = ""
        selectedRoleId = ""
        selectedJobId = ""
        selectedStartTime = System.currentTimeMillis()
        selectedEndTime = System.currentTimeMillis()

        CustomerRepository.getAllCustomers { customers ->
            this.customers = customers
            spinner_customer.adapter = ArrayAdapter(this, R.layout.view_drop_down_menu, customers.map { it.name })
        }

        spinner_customer.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onNothingSelected(parent: AdapterView<*>?) {
                showToast(message = "Nothing selected")
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                selectedCustomerId = customers[position].id
                ProjectRepository.getProjectsForCustomer(customers[position]) { projects ->
                    this@AddTrackingActivity.projects = projects
                    spinner_project.adapter = ArrayAdapter(this@AddTrackingActivity, R.layout.view_drop_down_menu, projects.map { it.name })
                }
            }
        }

        spinner_project.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onNothingSelected(p0: AdapterView<*>?) {
                showToast(message = "Nothing selected")
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                selectedProjectId = projects[position].id
                RoleRepository.getAllRoles { roles ->
                    this@AddTrackingActivity.roles = roles
                    spinner_role.adapter = ArrayAdapter(this@AddTrackingActivity, R.layout.view_drop_down_menu, roles.map { it.name })
                }
            }
        }

        spinner_role.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
                showToast(message = "Nothing selected")
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                selectedRoleId = roles[position].id
                JobRepository.getAllJobs { tasks ->
                    this@AddTrackingActivity.tasks = tasks
                    spinner_task.adapter = ArrayAdapter(this@AddTrackingActivity, R.layout.view_drop_down_menu, tasks.map { it.name })
                }
            }
        }

        spinner_task.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
                showToast(message = "Nothing selected")
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                selectedJobId = tasks[position].id
            }
        }

        val showStartDate: TextView = findViewById(R.id.show_start_date)
        val simpleDateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.GERMANY)
        simpleDateFormat.timeZone = TimeZone.getTimeZone("GMT+2:00")

        showStartDate.text = simpleDateFormat.format(System.currentTimeMillis())


        val startDateSetListener = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            calendarStart.set(Calendar.YEAR, year)
            calendarStart.set(Calendar.MONTH, monthOfYear)
            calendarStart.set(Calendar.DAY_OF_MONTH, dayOfMonth)

            showStartDate.text = simpleDateFormat.format(calendarStart.time)
        }

        showStartDate.setOnClickListener {
            DatePickerDialog(this@AddTrackingActivity, startDateSetListener,
                    calendarStart.get(Calendar.YEAR),
                    calendarStart.get(Calendar.MONTH),
                    calendarStart.get(Calendar.DAY_OF_MONTH)).show()
        }

        val showStartTime: TextView = findViewById(R.id.show_start_time)
        val simpleTimeFormat = SimpleDateFormat("HH:mm", Locale.GERMANY)
        simpleTimeFormat.timeZone = TimeZone.getTimeZone("GMT+2:00")

        showStartTime.text = simpleTimeFormat.format(System.currentTimeMillis())

        val startTimeSetListener = TimePickerDialog.OnTimeSetListener { _, hour, minute ->
            calendarStart.set(Calendar.HOUR, hour)
            calendarStart.set(Calendar.MINUTE, minute)

            showStartTime.text = simpleTimeFormat.format(calendarStart.time)
            selectedStartTime = calendarStart.timeInMillis
        }

        showStartTime.setOnClickListener {
            TimePickerDialog(this@AddTrackingActivity, startTimeSetListener,
                    calendarStart.get(Calendar.HOUR),
                    calendarStart.get(Calendar.MINUTE), true).show()
        }

        val showEndDate: TextView = findViewById(R.id.show_end_date)
        showEndDate.text = SimpleDateFormat("dd.MM.yyyy", Locale.GERMANY).format(System.currentTimeMillis())

        val endDateSetListener = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            calendarEnd.set(Calendar.YEAR, year)
            calendarEnd.set(Calendar.MONTH, monthOfYear)
            calendarEnd.set(Calendar.DAY_OF_MONTH, dayOfMonth)

            showEndDate.text = simpleDateFormat.format(calendarEnd.time)
        }

        showEndDate.setOnClickListener {
            DatePickerDialog(this@AddTrackingActivity, endDateSetListener,
                    calendarEnd.get(Calendar.YEAR),
                    calendarEnd.get(Calendar.MONTH),
                    calendarEnd.get(Calendar.DAY_OF_MONTH)).show()
        }

        val showEndTime: TextView = findViewById(R.id.show_end_time)
        showEndTime.text = simpleTimeFormat.format(System.currentTimeMillis())

        val endTimeSetListener = TimePickerDialog.OnTimeSetListener { _, hour, minute ->
            calendarEnd.set(Calendar.HOUR, hour)
            calendarEnd.set(Calendar.MINUTE, minute)

            showEndTime.text = simpleTimeFormat.format(calendarEnd.time)
            selectedEndTime = calendarEnd.timeInMillis
        }

        showEndTime.setOnClickListener {
            TimePickerDialog(this@AddTrackingActivity, endTimeSetListener,
                    calendarEnd.get(Calendar.HOUR),
                    calendarEnd.get(Calendar.MINUTE), true).show()

        }

        btn_send_tracking_data.setOnClickListener {

            val trackingRequestBody: RequestBody = createJsonRequestBody("userId" to "c5c3e968-6cea-467b-aaaa-e7d5ef186784", "customerId" to selectedCustomerId, "projectId" to selectedProjectId,
                    "roleId" to selectedRoleId, "jobId" to selectedJobId, "startTime" to selectedStartTime / 1000, "endTime" to selectedEndTime / 1000)

            TrackingRepository.addTrackingForUser(trackingRequestBody, { response ->
                showToast(message = TrackingRepository.getDateTime(selectedStartTime))
            })
        }

    }
}


