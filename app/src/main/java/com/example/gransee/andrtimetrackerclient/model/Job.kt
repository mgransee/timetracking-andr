package com.example.gransee.andrtimetrackerclient.model

import com.example.gransee.andrtimetrackerclient.model.response.JobResponse

/**
 * Created by gransee on 01.10.2018.
 */
class Job(jobResponse: JobResponse) {

    val id: String = jobResponse.id
    val name: String = jobResponse.name
}