package com.example.gransee.andrtimetrackerclient.model

import com.example.gransee.andrtimetrackerclient.model.response.ProjectResponse

/**
 * Created by gransee on 02.10.2018.
 */
class Project(projectResponse: ProjectResponse) {

    val id: String = projectResponse.id
    val name: String = projectResponse.name
}