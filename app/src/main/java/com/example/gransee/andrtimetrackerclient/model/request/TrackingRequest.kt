package com.example.gransee.andrtimetrackerclient.model.request

/**
 * Created by gransee on 09.10.2018.
 */
class TrackingRequest(val userId:String, val customerId:String, val projectId:String,
                      val roleId:String, val jobId: String, val startTime:Long, val endTime: Long)