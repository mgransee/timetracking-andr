package com.example.gransee.andrtimetrackerclient.network

import com.example.gransee.andrtimetrackerclient.model.Tracking
import com.example.gransee.andrtimetrackerclient.model.response.*
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.http.*

interface ApiService {

    //tracking API

    @GET("trackings")
    fun getTrackings(): Call<List<TrackingResponse>>

    @GET("trackings/id/{id}")
    fun getTrackingForId(@Path("id") id: String): Call<TrackingResponse>

    @GET("trackings/user/{user}")
    fun getTrackingsForUser(@Path("user") user: String): Call<List<TrackingResponse>>

    @POST("trackings")
    fun sendTrackingData(@Body body: RequestBody): Call<TrackingResponse>

    //project API

    @GET("project")
    fun getProjects(): Call<List<ProjectResponse>>

    @GET("project/customer/{customer}")
    fun getProjectsForCustomer(@Path("customer") customer: String): Call<List<ProjectResponse>>

    //customer API

    @GET("customer")
    fun getCustomers(): Call<List<CustomerResponse>>

    //role API

    @GET("role")
    fun getRoles(): Call<List<RoleResponse>>

    // task API

    @GET("job")
    fun getJobs(): Call<List<JobResponse>>
}