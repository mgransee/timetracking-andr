package com.example.gransee.andrtimetrackerclient.view

import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.TextView
import com.example.gransee.andrtimetrackerclient.R
import kotlinx.android.synthetic.main.activity_main.*
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by gransee on 24.09.2018.
 */
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn_trackings.setOnClickListener {
            val intent = Intent(this, TrackingsActivity::class.java)
            startActivity(intent)
        }

        btn_add_tracking.setOnClickListener {
            val intent = Intent(this, AddTrackingActivity::class.java)
            startActivity(intent)
        }
    }
}