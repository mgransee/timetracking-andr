package com.example.gransee.andrtimetrackerclient.repositories

import com.example.gransee.andrtimetrackerclient.model.Job
import com.example.gransee.andrtimetrackerclient.model.response.JobResponse
import com.example.gransee.andrtimetrackerclient.network.Network
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by gransee on 01.10.2018.
 */
object JobRepository {

    fun getAllJobs(onSuccess: (List<Job>) -> Unit) {
        Network.getService().getJobs().enqueue(object : Callback<List<JobResponse>> {
            override fun onResponse(call: Call<List<JobResponse>>?, response: Response<List<JobResponse>>?) {
                if (response != null && response.isSuccessful) {
                    onSuccess(response.body()?.map { Job(it) } ?: emptyList())
                }


            }

            override fun onFailure(call: Call<List<JobResponse>>?, t: Throwable?) {

            }
        })
    }
}