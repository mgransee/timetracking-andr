package com.example.gransee.andrtimetrackerclient.model.response

/**
 * Created by gransee on 02.10.2018.
 */
data class ProjectResponse(val id: String, val name: String, val customerId: String)