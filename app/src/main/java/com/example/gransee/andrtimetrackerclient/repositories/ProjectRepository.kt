package com.example.gransee.andrtimetrackerclient.repositories

import com.example.gransee.andrtimetrackerclient.model.Customer
import com.example.gransee.andrtimetrackerclient.model.Project
import com.example.gransee.andrtimetrackerclient.model.response.ProjectResponse
import com.example.gransee.andrtimetrackerclient.network.Network
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by gransee on 02.10.2018.
 */
object ProjectRepository {

    fun getAllProjects(onSuccess: (List<Project>) -> Unit) {
        Network.getService().getProjects().enqueue(object : Callback<List<ProjectResponse>> {
            override fun onResponse(call: Call<List<ProjectResponse>>?, response: Response<List<ProjectResponse>>?) {
                if (response != null && response.isSuccessful) {
                    onSuccess(response.body()?.map { Project(it) } ?: emptyList())
                }

            }

            override fun onFailure(call: Call<List<ProjectResponse>>?, t: Throwable?) {

            }
        })
    }

    fun getProjectsForCustomer(customer: Customer, onSuccess: (List<Project>) -> Unit) {
        Network.getService().getProjectsForCustomer(customer.id).enqueue(object: Callback<List<ProjectResponse>> {
            override fun onResponse(call: Call<List<ProjectResponse>>?, response: Response<List<ProjectResponse>>?) {
                if (response != null && response.isSuccessful) {
                    onSuccess(response.body()?.map {Project(it)}?: emptyList())
                }
            }

            override fun onFailure(call: Call<List<ProjectResponse>>?, t: Throwable?) {
                onSuccess(emptyList())
            }
        })
    }
}