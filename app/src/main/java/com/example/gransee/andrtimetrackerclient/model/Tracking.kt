package com.example.gransee.andrtimetrackerclient.model

import com.example.gransee.andrtimetrackerclient.model.response.TrackingResponse

/**
 * Created by gransee on 02.10.2018.
 */
class Tracking(trackingResponse: TrackingResponse,
    val id: String=trackingResponse.id,
    val user: String=trackingResponse.user,
    val customer: String=trackingResponse.customer,
    val project: String=trackingResponse.project,
    val role: String=trackingResponse.role,
    val job: String=trackingResponse.job,
    val start: Long=trackingResponse.start,
    val end: Long=trackingResponse.end)
