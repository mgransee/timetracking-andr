package com.example.gransee.andrtimetrackerclient.model.response

/**
 * Created by gransee on 01.10.2018.
 */
data class JobResponse(val id: String, val name: String)