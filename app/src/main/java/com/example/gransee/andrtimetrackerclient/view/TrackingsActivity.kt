package com.example.gransee.andrtimetrackerclient.view

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.example.gransee.andrtimetrackerclient.R
import com.example.gransee.andrtimetrackerclient.repositories.TrackingRepository
import kotlinx.android.synthetic.main.activity_trackings.*

class TrackingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trackings)

        btn_search_trackings.setOnClickListener {
            TrackingRepository.getTrackingsForUser { trackings ->
                tracking_list.adapter = TrackingAdapter(trackings)
                tracking_list.layoutManager = LinearLayoutManager(this)
            }
        }
    }
}
