package com.example.gransee.andrtimetrackerclient.repositories

import com.example.gransee.andrtimetrackerclient.model.Role
import com.example.gransee.andrtimetrackerclient.model.response.RoleResponse
import com.example.gransee.andrtimetrackerclient.network.Network
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by gransee on 01.10.2018.
 */
object RoleRepository {

    fun getAllRoles(onSuccess: (List<Role>) -> Unit) {
        Network.getService().getRoles().enqueue(object : Callback<List<RoleResponse>> {
            override fun onResponse(call: Call<List<RoleResponse>>?, response: Response<List<RoleResponse>>?) {
                if (response != null && response.isSuccessful) {
                    onSuccess(response.body()?.map { Role(it) } ?: emptyList() )
                }

            }

            override fun onFailure(call: Call<List<RoleResponse>>?, t: Throwable?) {

            }
        })
    }
}