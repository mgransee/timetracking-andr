package com.example.gransee.andrtimetrackerclient.model.response

data class CustomerResponse(val id: String, val name: String)