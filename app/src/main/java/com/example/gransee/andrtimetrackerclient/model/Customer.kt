package com.example.gransee.andrtimetrackerclient.model

import com.example.gransee.andrtimetrackerclient.model.response.CustomerResponse

/**
 * Created by gransee on 27.09.2018.
 */
class Customer(customerResponse: CustomerResponse) {

    val id: String = customerResponse.id
    val name: String = customerResponse.name

}