package com.example.gransee.andrtimetrackerclient.model

import com.example.gransee.andrtimetrackerclient.model.response.RoleResponse

/**
 * Created by gransee on 01.10.2018.
 */
class Role(roleResponse: RoleResponse) {

    val id: String = roleResponse.id
    val name: String = roleResponse.name

}