package com.example.gransee.andrtimetrackerclient.repositories

import com.example.gransee.andrtimetrackerclient.model.Customer
import com.example.gransee.andrtimetrackerclient.model.response.CustomerResponse
import com.example.gransee.andrtimetrackerclient.network.Network
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.http.GET

/**
 * Created by gransee on 27.09.2018.
 */
object CustomerRepository {

    fun getAllCustomers(onSuccess: (List<Customer>) -> Unit) {
        Network.getService().getCustomers().enqueue(object : Callback<List<CustomerResponse>> {
            override fun onResponse(call: Call<List<CustomerResponse>>?, response: Response<List<CustomerResponse>>?) {
                if (response != null && response.isSuccessful) {
                    onSuccess(response.body()?.map { Customer(it) } ?: emptyList())
                }

            }

            override fun onFailure(call: Call<List<CustomerResponse>>?, t: Throwable?) {

            }
        })
    }
}