package com.example.gransee.andrtimetrackerclient.model.response

data class TrackingResponse(val id: String, val user: String, val project: String, val customer: String, val role: String, val job: String, val start: Long, val end: Long)
