package com.example.gransee.andrtimetrackerclient.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import java.util.*

/**
 * Created by gransee on 05.10.2018.
 */
fun ViewGroup.inflate(layoutRes: Int): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, false)
}

fun Calendar.timeInSec(): Long {
    return (this.timeInMillis)/1000
}