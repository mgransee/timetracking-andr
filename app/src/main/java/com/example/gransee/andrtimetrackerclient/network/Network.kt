package com.example.gransee.andrtimetrackerclient.network

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object Network {

    fun getService(): ApiService {

        val retrofit = Retrofit.Builder()
                .addConverterFactory(
                        GsonConverterFactory.create())
                .baseUrl("http://192.168.188.41:3000/api/v1/")
                .build()

        return retrofit.create(ApiService::class.java)
    }

}