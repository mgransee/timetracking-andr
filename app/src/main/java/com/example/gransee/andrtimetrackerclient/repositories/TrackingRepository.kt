package com.example.gransee.andrtimetrackerclient.repositories

import com.example.gransee.andrtimetrackerclient.model.Tracking
import com.example.gransee.andrtimetrackerclient.model.request.TrackingRequest
import com.example.gransee.andrtimetrackerclient.model.response.TrackingResponse
import com.example.gransee.andrtimetrackerclient.network.Network
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by gransee on 02.10.2018.
 */
object TrackingRepository {

    fun getDateTime(timestamp: Long?): String {
        val simpleDateTimeFormat = SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.GERMANY)
        simpleDateTimeFormat.timeZone= TimeZone.getTimeZone("GMT+2:00")
        return if (timestamp != null) {
            val dateTime = Date(timestamp)
            simpleDateTimeFormat.format(dateTime)
        } else {
            ""
        }
    }

    fun getTrackingsForUser(onSuccess: (List<Tracking>) -> Unit) {

        Network.getService().getTrackingsForUser("c5c3e968-6cea-467b-aaaa-e7d5ef186784").enqueue(object : Callback<List<TrackingResponse>> {

            override fun onResponse(call: Call<List<TrackingResponse>>?, response: Response<List<TrackingResponse>>?) {
                if (response != null && response.isSuccessful) {
                    onSuccess(response.body()?.map { Tracking(it) } ?: emptyList())
                }
            }

            override fun onFailure(call: Call<List<TrackingResponse>>?, t: Throwable?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }
        })
    }

    fun addTrackingForUser(requestBody: RequestBody, onSuccess: (TrackingResponse?) -> Unit) {

        Network.getService().sendTrackingData(requestBody)
                .enqueue(object : Callback<TrackingResponse> {
                    override fun onResponse(call: Call<TrackingResponse>, response: Response<TrackingResponse>?) {
                        if (response != null && response.isSuccessful) {
                            onSuccess(response.body())
                        }
                    }

                    override fun onFailure(call: Call<TrackingResponse>?, t: Throwable?) {
                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                    }
                })
    }
}



