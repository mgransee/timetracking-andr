package com.example.gransee.andrtimetrackerclient.view

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.gransee.andrtimetrackerclient.R
import com.example.gransee.andrtimetrackerclient.model.Tracking
import com.example.gransee.andrtimetrackerclient.repositories.TrackingRepository.getDateTime

class TrackingAdapter(private val items: List<Tracking>?) : RecyclerView.Adapter<TrackingAdapter.TrackingViewHolder>() {

    override fun onBindViewHolder(holder: TrackingViewHolder, position: Int) {

        if (items != null) {
            val item = items.get(position)

            holder.customer?.text = "Kunde: " + item.customer
            holder.project?.text = "Projekt: " + item.project
            holder.roleTitle?.text = "Rolle: " + item.role
            holder.jobTitle?.text = "Aufgabe: " + item.job
            holder.startTime?.text = "Beginn: " + getDateTime(item.start*1000)
            holder.endTime?.text = "Ende: " + getDateTime(item.end*1000)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TrackingViewHolder {
        val view = parent.inflate(R.layout.item_tracking)
        return TrackingViewHolder(view)
    }


    override fun getItemCount(): Int {
        return items?.size ?: 0
    }

    class TrackingViewHolder(
            itemView: View,
            val customer: TextView? = itemView.findViewById(R.id.customer),
            val project: TextView? = itemView.findViewById(R.id.project),
            val roleTitle: TextView? = itemView.findViewById(R.id.role),
            val jobTitle: TextView? = itemView.findViewById(R.id.job),
            val startTime: TextView? = itemView.findViewById(R.id.start),
            val endTime: TextView? = itemView.findViewById(R.id.end)
    ) : RecyclerView.ViewHolder(itemView)
}